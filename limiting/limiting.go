package limiting

import (
	"fmt"
	"strconv"
	"sync/atomic"
	"time"

	"bitbucket.region.vtb.ru/rlw/utils"
)

type Limiter interface {
	Start()
	IsRequestLimited() bool
}

type FixedWindow struct {
	window  *utils.TimeDuration
	counter uint64
	limit   uint64
}

func NewFixedWindow(window *utils.TimeDuration, limit uint64) Limiter {
	return &FixedWindow{window: window, counter: 0, limit: limit}
}

func (fw *FixedWindow) Start() {
	ticker := time.NewTicker(time.Duration(fw.window.Nanosecond()))
	go func() {
		for t := range ticker.C {
			fmt.Printf("Update counter. Current time [%v] tick [%v]\n", time.Now(), t)
			atomic.StoreUint64(&fw.counter, 0)
		}
		// for {

		// fw.window.Sleep()
		// }
	}()
}

func (fw *FixedWindow) IsRequestLimited() bool {
	if atomic.LoadUint64(&fw.counter) < fw.limit /*счетчик не достиг лимита*/ {
		atomic.AddUint64(&fw.counter, 1)
		fmt.Printf("|----------PASSED----------|\n\n")
		return false
	} else {
		fmt.Printf("|----------LIMITED----------|\n\n")
		return true
	}
}

type SlidingWindow struct {
	//Показывает какими интервалами дробить окно(в какие части распределять запросы).
	// Чем больше размер окна, тем больше гранулярность(пока не используется)
	granularity     time.Duration
	window          *utils.TimeDuration
	requestPerCount map[int64]int64
	limit           int64
	total_passed    int64
	total_limited   int64
}

func NewSlidingWindow(window *utils.TimeDuration, limit int64) Limiter {
	return &SlidingWindow{granularity: time.Nanosecond, window: window, requestPerCount: make(map[int64]int64),
		limit: limit}
}

func (sw *SlidingWindow) Start() {
	//do nothing
	//  можно добавить тикер, который будет показывать сколько времени со старта прошло
}

func (sw *SlidingWindow) IsRequestLimited() bool {
	//берем время когда пришел запрос
	current := time.Now().UnixNano()
	//подсчитываем для конкретного запроса время начала окна
	start := current - sw.window.Nanosecond()

	passed_requests_count := 0
	//проходимся по кэшу запросов
	for ts, counter := range sw.requestPerCount {
		if ts > start {
			// если время предыдущих запросов из кэша попадает в окно под текущий запрос - увеличиваем счетчик запросов попавших в окно для текущего запроса
			passed_requests_count += int(counter)
		} else {
			// чистим старые запросы(которые не попали в последнее окно) из кэша
			delete(sw.requestPerCount, ts)
		}
	}
	if passed_requests_count > int(sw.limit) {
		//Если количество сделанных запросов в окне выползает за установленный лимит - отбрасываем текущий запрос
		sw.total_limited += 1
		fmt.Printf("|----------LIMITED----------|\n request [%v] \n window  [%v]\n passed [%v]\n limited [%v]\n\n",
			strconv.FormatInt(current, 30), strconv.FormatInt(start, 30), sw.total_passed, sw.total_limited)
		return false
	}
	//разрешаем запросу пройти и увеличиваем счетчик
	sw.requestPerCount[current] += 1
	sw.total_passed += 1
	fmt.Printf("|----------PASSED-----------|\n request [%v] \n window  [%v]\n passed [%v]\n limited [%v]\n\n\n",
		strconv.FormatInt(current, 30), strconv.FormatInt(start, 30), sw.total_passed, sw.total_limited)
	return true
}
