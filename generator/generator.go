package generator

import "time"

type Generator struct {
	pause    time.Duration
	payload  []byte
	consumer *Consumer
}

func NewGenerator(pause time.Duration, payload []byte, consumer *Consumer) *Generator {
	return &Generator{pause: pause, payload: payload, consumer: consumer}
}

func (g *Generator) Start() {
	go func() {
		for {
			c := *g.consumer
			c.Consume(g.payload)
			time.Sleep(g.pause * time.Second)
		}
	}()
}

type Consumer interface {
	Consume(payload []byte)
}
