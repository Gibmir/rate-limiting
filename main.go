package main

import (
	"time"

	"bitbucket.region.vtb.ru/rlw/generator"
	"bitbucket.region.vtb.ru/rlw/limiting"
	"bitbucket.region.vtb.ru/rlw/utils"
)

func main() {
	//разрешено 3 запроса на окно в 1 минуту
	limiter := limiting.NewSlidingWindow(utils.NewTimeDuration(1, time.Minute), 3)
	//разрешено два запроса в 10 секунд
	// limiter := limiting.NewFixedWindow(utils.NewTimeDuration(10, time.Second), 2)
	limiter.Start()
	var consumer generator.Consumer = &BlackHole{Limiter: limiter}
	g := generator.NewGenerator(7, []byte{'g', 'o', 'l', 'a', 'n', 'g'}, &consumer)
	g.Start()
	for {

	}
}

type BlackHole struct {
	Limiter limiting.Limiter
}

func (bh *BlackHole) Consume(payload []byte) {
	bh.Limiter.IsRequestLimited()
}
