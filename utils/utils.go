package utils

import "time"

type TimeDuration struct {
	t        int64
	duration time.Duration
}

func NewTimeDuration(t int64, unit time.Duration) *TimeDuration {
	return &TimeDuration{t: t, duration: unit}
}

func (td *TimeDuration) Sleep() {
	time.Sleep(time.Duration(td.t) * td.duration)
}

func (td *TimeDuration) Nanosecond() int64 {
	return int64(time.Duration(td.t) * td.duration)
}
